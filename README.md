# C Module For Lua

- 下载地址：[lua-5.4.4.tar.gz](https://www.lua.org/ftp/lua-5.4.4.tar.gz)
- 在线文档：[Lua 5.4 Reference Manual](https://www.lua.org/manual/5.4/)
- SHA 1 校验值：`03c27684b9d5d9783fb79a7c836ba1cdc5f309cd`

## 构建可动态加载 C 共享库的 Lua 解释器

```makefile
# 1. 解压 lua-5.4.4.tar.gz 文件得到 lua-5.4.4 目录
# 2. 编辑 lua-5.4.4/src/Makefile 文件，然后在 CFLAGS 和 LDFLAGS 变量的所在行后面添加以下内容
# 3. 调用 make linux 或 make linux-readline 构建 Lua 解释器

CFLAGS += -fpie
LDFLAGS += -pie
```

## 为 Lua 实现 C 模块共享库

```c
// 1. 需要导入的 Lua 头文件至少有 3 个，其他头文件的导入则根据需求来决定
// 2. 需要声明 luaL_Reg 类型数组来导出模块内容，结尾必须是 2 个 NULL 值，从而能够确定结束位置
// 3. 需要声明一个以 luaopen_ 开头的函数，允许 Lua 导入 C 模块共享库，函数名后续字符串则成为了 C 模块共享库的名称
// 4. 导出给 Lua 的 C 函数签名必须符合 Lua 声明的 lua_CFunction 函数签名

// lua 5.4.4
// lexample.c

#include <stdio.h>

#include "lua.h"
#include "luaconf.h"
#include "lauxlib.h"

static int l_cfun(lua_State *L)
{
	printf("cfun: Hi\n");

	return 0;
}

static const struct luaL_Reg lexample[] = {
	{"cfun", l_cfun},
	{NULL, NULL},
};

int luaopen_lexample(lua_State *L)
{
	luaL_newlib(L, lexample);
	return 1;
}
```

```makefile
# 构建 Lua 的 C 模块时，需要构建成共享库形式，以便能够动态加载链接

.PHONY: run clean

run: lexample.so
	@./lua example.lua

lexample.so: lexample.c
	gcc -c lexample.c -o lexample.o
	gcc -shared lexample.o -o lexample.so

clean:
	rm -rf *.o *.so
```

```lua
-- example.lua

local example = require "lexample"

print(example)
example.cfun()
```
