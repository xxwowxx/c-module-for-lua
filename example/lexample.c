// lua 5.4.4

#include <stdio.h>

#include "lua.h"
#include "luaconf.h"
#include "lauxlib.h"

static int l_cfun(lua_State *L)
{
	printf("cfun: Hi\n");

	return 0;
}

static const struct luaL_Reg lexample[] = {
	{"cfun", l_cfun},
	{NULL, NULL},
};

int luaopen_lexample(lua_State *L)
{
	luaL_newlib(L, lexample);
	return 1;
}
